<?php

$protocol = isset($_SERVER['HTTPS']) ? "https" : "http";

$rain_kml = "$protocol://google.meteox.com/meteox.kmz?time=" . @mktime();

echo <<<EOS1
	<STYLE type="text/css">
	/*<![CDATA[*/
		div#content	{ text-align:left; }
		div#content img	{ float:right; }
		html, body      { height: 100%; }	/* needed for keyboard zoom to work in Firefox */
	/*]]>*/
	</STYLE>
	<SCRIPT src="$protocol://maps.google.com/maps/api/js?sensor=false"></SCRIPT>
	
	<SCRIPT type="text/javascript">
	// <![CDATA[
	
	var map;
	var rain;
	var show_rain = false;
	var show_shading = false;
	var show_contours = false;
	var zoom_out = false;

	var mapnikMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenStreetMap Mapnik",
		name: "OSM-MP",
		maxZoom: 18
	});
	var cycleMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tile.opencyclemap.org/cycle/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenStreetMap Cycle",
		name: "OSM-Cycle",
		maxZoom: 18
	});

	var opentopomapMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tile.opentopomap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenTopoMap",
		name: "OSM-Topo",
		minZoom: 5,
		maxZoom: 15
	});

	var mapquestMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://otile1.mqcdn.com/tiles/1.0.0/osm/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "Mapquest",
		name: "Mapquest",
		maxZoom: 17
	});

	var reliefMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://www.maps-for-free.com/layer/relief/z" + zoom + "/row" + coord.y + "/" + zoom + "_" + coord.x + "-" + coord.y + ".jpg";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: false,
		alt: "Relief",
		name: "Relief",
		maxZoom: 11
	});

	// Overlays: see: http://wiki.openstreetmap.org/wiki/Tileserver
	var hillshadingMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tiles.wmflabs.org/hillshading/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenStreepMap SRTM",
		name: "OSM-SRTM",
		maxZoom: 16
	});

	var contoursMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://www.wanderreitkarte.de/hills/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true
	});

	var contours3MapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://contour.heywhatsthat.com/bin/contour_tiles.cgi?src=contourmapplet&color=" + 83422530 + "&interval=" + 100 + "&zoom=" + zoom + "&x=" + coord.x + "&y=" + coord.y;
		},
		tileSize: new google.maps.Size(256, 256),
	});

	var contours2MapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			var projection = map.getProjection();
			var zpow = Math.pow(2, zoom);
			var ul = new google.maps.Point(coord.x * 600.0 / zpow, (coord.y + 1) * 600.0 / zpow);
			var lr = new google.maps.Point((coord.x + 1) * 600.0 / zpow, (coord.y) * 600.0 / zpow);
			var ulw = projection.fromPointToLatLng(ul);
			var lrw = projection.fromPointToLatLng(lr);
			//The user will enter the address to the public WMS layer here.  The data must be in WGS84
			var baseURL = "$protocol://geoweb.hft-stuttgart.de/cgi-bin/mapserv?map=/home/fjbehr/SRTM/test.map";
			var version = "1.1.1";
			var request = "GetMap";
			var format = "image%2Fgif"; //type of image returned  or image/jpeg
			//The layer ID.  Can be found when using the layers properties tool in ArcMap or from the WMS settings 
			var layers = "Contourlinien100";
			//projection to display. This is the projection of google map. Don't change unless you know what you are doing.  
			//Different from other WMS servers that the projection information is called by crs, instead of srs
			var crs = "EPSG:4326"; 
			//With the 1.3.0 version the coordinates are read in LatLon, as opposed to LonLat in previous versions
			var bbox = ulw.lat() + "," + ulw.lng() + "," + lrw.lat() + "," + lrw.lng();
			var service = "WMS";
			//the size of the tile, must be 256x256
			var width = "600";
			//var width = "256";
			var height = "600";
			//var height = "256";
			//Some WMS come with named styles.  The user can set to default.
			var styles = "default";
			var transparency = "true";
			//Establish the baseURL.  Several elements, including &EXCEPTIONS=INIMAGE and &Service are unique to openLayers addresses.
			var url = baseURL + "&Layers=" + layers + "&version=" + version + "&EXCEPTIONS=INIMAGE" + "&service=" + service + "&request=" + request + "&Styles=" + styles + "&format=" + format + "&SRS=" + crs + "&BBOX=" + bbox + "&width=" + width + "&height=" + height + "&transparent=" + transparency;

			return url;
		},
		tileSize: new google.maps.Size(600, 600),
		isPng: false
	});

	var tileHybridLabels = new google.maps.ImageMapType({
		getTileUrl: function(ll, z) {
			var k = (ll.x + ll.y) % 4;
			var X = ll.x % (1 << z);  // wrap
			var e = (ll.x*3+ll.y)%8;
			var s = "Galileo".substr(0,e);
			return "$protocol://mt" + k + ".google.com/vt/imgtp=png32&lyrs=h&hl=en-US&x=" + X + "&y=" + ll.y + "&z=" + z + "&s=" + s;
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		maxZoom: 20,
		name: "hybrid",
		alt: "Hybrid Labels"
	});

	// see: http://openweathermap.org/hugemaps
	var owmprecipitationMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tile.openweathermap.org/map/precipitation/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenWeatherMap Precipitation",
		name: "OWM-Rain",
		opacity: 0.5,
		maxZoom: 19
	});
	
	var owmpressureMapType = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			return "$protocol://tile.openweathermap.org/map/pressure_cntr/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		alt: "OpenWeatherMap Pressure",
		name: "OWM-press",
		maxZoom: 19
	});

	
	var points = new Array();
	var accuracy = new Array();
	var texts = new Array();
	var times = new Array();
	var mapimages = new Array();
	var mapimagewidths = new Array();
	var weather = new Array();
	var marker = new Array();
	var acccircle = new Array();
	var infowindow = new google.maps.InfoWindow();
	var iwcontent = new Array();
	var autopos = new Array();
	var newseg = new Array();
	var seglength = new Array();
	var polyline;
	var kml;

	function resize_map_to_full_window()
	{
		document.getElementById("map").style.position = "absolute";
		document.getElementById("map").style.left = "0px";
		document.getElementById("map").style.width = window.innerWidth + "px";
		document.getElementById("map").style.height = window.innerHeight + "px";
		document.getElementById("map").style.marginBottom = "0px";
	}

	function deg2rad(deg)
	{
		return deg / 180 * Math.PI;
	}

	function calc_dist(lat1, lon1, lat2, lon2)
	{
		return 6371 * Math.acos( Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(lon2 - lon1)) );
	}

	function create_map()
	{
		var mapCenter;

		// set body width to maximum and disable/enable scrolling, to get scrollbar width
		maxWidth = getComputedStyle( document.body ).maxWidth;
		document.body.style.maxWidth = "none";
		document.body.style.overflow = "hidden";
		innerWidth1 = parseInt( getComputedStyle( document.body ).width );
		document.body.style.overflow = "initial";
		innerWidth2 = parseInt( getComputedStyle( document.body ).width );
		document.body.style.maxWidth = maxWidth;
		scrollbarWidth = innerWidth1 - innerWidth2;
		
		// reset maxWidth and Width from the values inherited from body, before creating the map
		document.getElementById("map").style.maxWidth = (window.innerWidth - scrollbarWidth) + "px";
		document.getElementById("map").style.width = (window.innerWidth - scrollbarWidth) + "px";

		map = new google.maps.Map( document.getElementById( "map" ) );
		
		if( $use_kml )
		{
			kml = new google.maps.KmlLayer( '$kml_file' );
			kml.setMap( map );
			kml.set( 'preserveViewport', true ); 
		}

		map.overlayMapTypes.insertAt( 0, hillshadingMapType );
		map.overlayMapTypes.insertAt( 0, hillshadingMapType );
		show_shading = true;

		// contours deactivated by default, since they are a bit opaque and not globally available
		show_contours = false;

		var color_index = 0;
		var colors = [ "#FF004F", "#fFcF00" ];

		if( points && 0 != points.length )
		{
			mapCenter = new google.maps.LatLng( points[points.length - 1][0], points[points.length - 1][1] );
			
			if( points.length >= 2 )
			{

				for( var i in points )
				{
					if( i == 0 || newseg[i] )
					{
						if( i > 0 )
						{
							polyline.setMap( map );
						}

						polyline = new google.maps.Polyline({
							strokeColor: colors[color_index],
							strokeOpacity: 1.0,
							strokeWeight: 3
						});

						if( i > 0 )
						{
							polyline.getPath().push( new google.maps.LatLng( points[i-1][0], points[i-1][1] ) );
						}

						color_index++;
						color_index %= colors.length;
					}
					else
					{
						seglength[i] = calc_dist( points[i-1][0], points[i-1][1], points[i][0], points[i][1] );
					}

					polyline.getPath().push( new google.maps.LatLng( points[i][0], points[i][1] ) );
				}

				polyline.setMap( map );

				mapCenter = new google.maps.LatLng( ( points[points.length - 1][0] + points[points.length - 2][0] ) / 2, ( points[points.length - 1][1] + points[points.length - 2][1] ) / 2 );
			}

			color_index = 0;
			var sumlength = 0;

			for( var i in points )
			{
				if( newseg[i] )
				{
						color_index++;
						color_index %= colors.length;
				}

				// Punkte weglassen, wenn sie zu dicht aneinander liegen
				// 1. wenn vorher ein Marker ist, und zu dicht
				// 2. wenn danach ein Marker ist, und zu dicht
				// 3. wenn die aufsummierte Entfernung zu kurz
				if( autopos[i] &&
					i > 0 &&
					i < points.length &&
					((!autopos[i-1] && seglength[i] < 0.75) ||
					(!autopos[i+1] && seglength[i+1] < 0.75) ||
					(sumlength + seglength[i] < 0.75))
				 )
				{
					// Entfernung seit letztem Punkt
					sumlength += seglength[i];
					continue;
				}

				sumlength = 0;

				// Kernidee: mehrere Marker, mehrere Inhalte, aber nur ein Infowindow, so dass es wandert, anstatt mehrere Fenster offen zu haben
				marker[i] = new google.maps.Marker( autopos[i] || (newseg[i] && texts[i] === "") ?
					{
						position: new google.maps.LatLng( points[i][0], points[i][1] ),
						//title: times[i],
						title: texts[i].replace(/<br \/>/g, "\\n").replace(/&nbsp;/g, " ").replace(/&ouml;/g, "ö"),	// + "\\n#" + i + " l " + seglength[i],
						//clickable: false,
						icon: { path: google.maps.SymbolPath.CIRCLE, scale: 5, fillColor: colors[color_index], fillOpacity: 1, strokeColor: colors[color_index], strokeOpacity: 0, strokeWeight: 0 },
						map: map
					}
			       		:
					{
						position: new google.maps.LatLng( points[i][0], points[i][1] ),
						title: texts[i].replace(/<br \/>/g, "\\n")
							.replace(/<[^<>]+>/g, "")
							.replace(/&nbsp;/g, " ")
							.replace(/&auml;/g, "ä")
							.replace(/&ouml;/g, "ö")
							.replace(/&uuml;/g, "ü")
							.replace(/&Auml;/g, "Ä")
							.replace(/&Ouml;/g, "Ö")
							.replace(/&Uuml;/g, "Ü")
							.replace(/&szlig;/g, "ß")
							.replace(/&eacute;/g, "é")
							.replace(/&egrave;/g, "è")
							.replace(/&aacute;/g, "á")
							.replace(/&agrave;/g, "à")
							.replace(/&ccedil;/g, "ç")
							.replace(/&ndash;/g, "–")
							.replace(/&mdash;/g, "—")
							.replace(/&rsquo;/g, "’")
							.replace(/&bdquo;/g, "„")
							.replace(/&ldquo;/g, "“")
							.replace(/&rdquo;/g, "”")
							.replace(/&euro;/g, "€"),
						map: map
					}
					);

				// create infowindow only for manual points	
				if( !autopos[i] )
				{
					
					// accuracy: only if the points have not been sent automatically
					if( accuracy[i] )
					{
						acccircle[i] = new google.maps.Circle( {
								center: new google.maps.LatLng( points[i][0], points[i][1] ),
								radius: accuracy[i],
								fillColor: colors[color_index],
								fillOpacity: 0.25,
								strokeColor: colors[color_index],
								strokeOpacity: 0,
								strokeWeight: 0,
								map: map
						} );
					}
				
					// overflow: so the div container surrounds the image, and can be clicked to close the window
					iwcontent[i]  = '<div id="content" style="color:#000000; overflow:auto;" onClick="infowindow.close();"><div id="siteNotice" style="font-weight:bold;">' + times[i] + '</div>';
					iwcontent[i] += '<div id="bodyContent" style="text-align:left; margin-top:1ex;">';
					if(mapimagewidths[i] > 0) {
						iwcontent[i] += '<a href="#showimage"><img src="' + mapimages[i] + '" style="float:right; margin:10px; width:' + mapimagewidths[i] + 'px;" /></a>';
					}
					iwcontent[i] += texts[i];
					iwcontent[i] += ( weather[i].length ? '<p><i><b>Wetter:</b> ' + weather[i][0] + ', ' + weather[i][1] + '&nbsp;&deg;C, ' + weather[i][3] + '</i></p>' : '' );
					iwcontent[i] += '</div></div>';

					// if the marker is clicked, fill the infowindow and the light box with the appropriate content, and open the window
					google.maps.event.addListener( marker[i], 'click', (function( markerArg, index ) { return function() {
						infowindow.setContent( iwcontent[index] );
						document.getElementById('lightboximg').src = mapimages[index];
						infowindow.open( map, markerArg );
					} } )(marker[i], i));
				
				}
			}

		}
		else
		{
			mapCenter = new google.maps.LatLng( $midpoint );
		}
	
		var mapOptions = {
			zoom: 10,
			center: mapCenter,
			scaleControl: true,
			navigationControl: true,
			navigationControlOptions: { style: google.maps.NavigationControlStyle.DEFAULT },
			mapTypeId: 'OSM-MP',
			mapTypeControlOptions: {
  	  	  		mapTypeIds: ['OSM-MP', 'OSM-Cycle', 'OSM-Topo', 'Mapquest', 'Relief', google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.TERRAIN ],
				style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
			}
		};

		map.setOptions( mapOptions );
		map.mapTypes.set( 'Relief', reliefMapType );
		map.mapTypes.set( 'OSM-MP', mapnikMapType );
		map.mapTypes.set( 'OSM-Cycle', cycleMapType );
		map.mapTypes.set( 'OSM-Topo', opentopomapMapType );
		map.mapTypes.set( 'Mapquest', mapquestMapType );
		map.setMapTypeId( 'OSM-MP' );

		google.maps.event.addListener(map, 'maptypeid_changed', function() {

			if( map.mapTypeId == "Relief" )
			{
				map.overlayMapTypes.push( tileHybridLabels );
			}
			else
			{
				map.overlayMapTypes.pop();
			}
		});
		
		
		// add lightbox; as last node, so it overlays the map and all the rest
		var lightboxnode = document.createElement('div');
		lightboxnode.id = 'showimage';
		lightboxnode.className = 'lightbox';
		lightboxnode.appendChild( document.createElement('a') );
		lightboxnode.firstChild.onclick = function() { window.history.back(); };
		lightboxnode.firstChild.appendChild( document.createElement('img') );
		lightboxnode.firstChild.firstChild.id = 'lightboximg';
		document.getElementsByTagName("body")[0].appendChild( lightboxnode );
		document.onkeydown = function (e) { if( e.keyCode == 27 ) { if( document.location.hash == '#showimage' ) window.history.back(); else infowindow.close(); } };

		window.onresize = resize_map_to_full_window;
		resize_map_to_full_window();
	}

	function toggle_rain_kml()
	{
		if( show_rain == false )
		{
			if( ! rain )
			{
				rain = new google.maps.KmlLayer('$rain_kml');
				rain.set( 'preserveViewport', true ); 
			}

			rain.setMap( map );
			show_rain = true;
		}
		else
		{
			rain.setMap( null );
			show_rain = false;
		}
	}

	function toggle_rain_wms()
	{
		show_rain = show_rain ? false : true;
		renew_overlays();
	}

	function toggle_contours()
	{
		show_contours = show_contours ? false : true;
		renew_overlays();
	}

	function toggle_shading()
	{
		show_shading = show_shading ? false : true;
		renew_overlays();
	}

	function renew_overlays()
	{
		var current_center = map.getCenter();
		var current_zoom   = map.getZoom();

		map.overlayMapTypes.clear();

		if( show_shading == true )
			map.overlayMapTypes.push( hillshadingMapType );

		if( show_contours == true )
			map.overlayMapTypes.push( contoursMapType );

		if( show_rain == true )
		{
			map.overlayMapTypes.push( owmprecipitationMapType );
			map.overlayMapTypes.push( owmpressureMapType );
		}
		
		map.setCenter( current_center );
		map.setZoom( current_zoom );
	}

	function resize2track()
	{
		viewport = new google.maps.LatLngBounds( new google.maps.LatLng( Math.min.apply(Math, points.map(function(v) { return v[0]; })), Math.min.apply(Math, points.map(function(v) { return v[1]; })) ), new google.maps.LatLng( Math.max.apply(Math, points.map(function(v) { return v[0]; })), Math.max.apply(Math, points.map(function(v) { return v[1]; })) ) );

		map.fitBounds(viewport);
	}

	// attention: works only after KML layer is loaded
	// use event: google.maps.event.addListener( kml, 'metadata_changed', resize2kml );
	function resize2kml()
	{
		viewport = kml.getDefaultViewport();

		map.fitBounds(viewport);
	}

	function toggle_resize2kml()
	{
		if( zoom_out == false )
		{
			current_center = map.getCenter();
			current_zoom   = map.getZoom();

			if( kml )
			{
				resize2kml();
			}
			else
			{
				resize2track();
			}

			zoom_out = true;
		}
		else
		{
			map.setCenter( current_center );
			map.setZoom( current_zoom );
			zoom_out = false;
		}
	}

	// ]]>
	</SCRIPT>

EOS1;

echo "\t<SCRIPT type=\"text/javascript\">\n";
readfile( basename( $_SERVER['SCRIPT_NAME'], ".php" ) . ".js" );
echo "\t</SCRIPT>\n\n";

$layerswitcher_string = '<A href="#" target="_self" onClick="toggle_shading();return false;">Schattierung ein/ausschalten</A>' . ($use_kml ? ' | <A href="#" onClick="toggle_resize2kml();return false;">Zoom ganze Tour/Detail</A>' : '');
$layerswitcher_string_en = '<A href="#" target="_self" onClick="toggle_shading();return false;">shading on/off</A>' . ($use_kml ? ' | <A href="#" onClick="toggle_resize2kml();return false;">zoom whole tour/detail</A>' : '');
?>
